package com.sample.timetable.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

public class Timetable implements EntryPoint {

    private AbsolutePanel leftPanel = new AbsolutePanel();
    private AbsolutePanel rightPanel = new AbsolutePanel();
    private DecoratorPanel leftDecor = new DecoratorPanel();
    private DecoratorPanel rightDecor = new DecoratorPanel();
    private FlexTable leftFlexTable = new FlexTable();
    private FlexTable rightFlexTable = new FlexTable();
    private List<String> tasksL = new ArrayList<>();
    private List<String> tasksR = new ArrayList<>();
    private Label titleL = new Label();
    private Label titleR = new Label();
    private TextBox addtaskL = new TextBox();
    private TextBox addtaskR = new TextBox();
    private Button addTasksType = new Button();
    private Button addNewTask = new Button();

    public void onModuleLoad() {
        titleL.setText("Task type");
        titleR.setText("Tasks");

        addtaskL.setText("choose task"); // will be deleted
        addtaskR.setText("choose task"); // will be deleted
        addTasksType.setText("Add new task type");
        addNewTask.setText("Add new task");

        leftPanel.setPixelSize(300, 500);
        rightPanel.setPixelSize(600, 500);

        leftPanel.add(addtaskL, 10, 10);
        leftPanel.add(addTasksType, 10, 50);
        leftPanel.add(leftFlexTable, 10, 90);

        rightPanel.add(addtaskR, 10, 10);
        rightPanel.add(addNewTask, 10, 50);
        rightPanel.add(rightFlexTable, 10, 90);

        leftFlexTable.setCellPadding(6);
        rightFlexTable.setCellPadding(6);
        leftDecor.setWidget(leftPanel);
        rightDecor.setWidget(rightPanel);

        Grid grid = new Grid(3, 3);
        grid.setWidget(0, 1, titleL);
        grid.setWidget(0, 2, titleR);
        grid.setWidget(1, 1, leftDecor);
        grid.setWidget(1, 2, rightDecor);
        RootPanel.get().add(grid);

        addTasksType.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addTaskType(addtaskL, leftFlexTable, tasksL);
            }
        });

        addNewTask.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addTaskType(addtaskR, rightFlexTable, tasksR);
            }
        });
    }

    private void addTaskType(TextBox addtask, final FlexTable flexTable, List<String> tasks) {
        String symbol = addtask.getText().trim();
        addtask.setFocus(true);
        addtask.setText("");

        if (tasks.contains(symbol) || symbol.equals(""))
            return;

        int row = flexTable.getRowCount();
        tasks.add(symbol);
        flexTable.setText(row, 0, symbol);
        if (flexTable.equals(rightFlexTable)) {
            addButtons(symbol, row);
        }
        else if (flexTable.equals(leftFlexTable)) {
            clickLeftTableRow();
        }
    }

    private void addButtons(final String symbol, int row) {
        final Button doneTaskButton = new Button("v");
//            doneTaskButton.addStyleDependentName("default");
        doneTaskButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (doneTaskButton.getText().equals("v")) {
                    doneTaskButton.setText("Done");
//                        doneTaskButton.addStyleDependentName("done");
                } else {
                    doneTaskButton.setText("v");
//                        doneTaskButton.addStyleDependentName("default");
                }
            }
        });
        rightFlexTable.setWidget(row, 2, doneTaskButton);

        Button removeTaskButton = new Button("X");
        removeTaskButton.addStyleDependentName("remove");
        removeTaskButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                int removedTask = tasksR.indexOf(symbol);
                tasksR.remove(removedTask);
                rightFlexTable.removeRow(removedTask);
            }
        });
        rightFlexTable.setWidget(row, 3, removeTaskButton);
    }


    private void clickLeftTableRow() {
        leftFlexTable.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                int rowIndex = leftFlexTable.getCellForEvent(event).getRowIndex();
                String temp = tasksL.get(rowIndex);
                tasksL.add(temp);
                titleR.setText(temp);
            }
        });
    }
}

